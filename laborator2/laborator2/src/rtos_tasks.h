
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "freertos/semphr.h"

// defines
#define BITUL_X 0x01
#define BITUL_Y 0x04

// variables
static TaskHandle_t task1_handle = NULL;
static TaskHandle_t task2_handle = NULL;
static TaskHandle_t task1_ev_handle = NULL;
static TaskHandle_t task2_ev_handle = NULL;

EventGroupHandle_t grup;
SemaphoreHandle_t xSemaphore;


// functions
void task1();
void task2();
void task1_events();
void task2_events();

void task1_semaphore();
void task2_semaphore();