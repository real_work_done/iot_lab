#include "rtos_tasks.h"

void task1() {
  uint32_t ret;
  while(1) {
    ret=ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
    printf("Am primit notificarea nr. %d\n", ret);
  }
}

void task2() {
  uint32_t val=0;
  while(1) {
    vTaskDelay(1000 / portTICK_PERIOD_MS);
    xTaskNotify(task1_handle, val++, eSetValueWithOverwrite);
    vTaskDelay(pdMS_TO_TICKS( 300 ));
  }
}

void task1_events() {
  EventBits_t bits;
  while(1) {
    bits = xEventGroupWaitBits(grup, BITUL_X | BITUL_Y, pdTRUE, pdFALSE, 1000 / portTICK_PERIOD_MS);
    switch(bits) {
    case BITUL_X | BITUL_Y: printf("Ambii biti sunt setati\n"); break;
    case BITUL_X: printf("Bit-ul X este setat\n"); break;
    case BITUL_Y: printf("Bit-ul Y este setat\n"); break;
    default: printf("A mai trecut o secunda <%d> \n ", bits);
    }
  }
}


void task1_semaphore()
{
  while(1)
  {
    gpio_set_level(18, 0);
    gpio_set_level(19, 1);
    vTaskDelay(pdMS_TO_TICKS( 600 ));
  }
}
void task2_semaphore()
{
  while(1)
  {
    gpio_set_level(19, 0);
    gpio_set_level(18, 1);
    vTaskDelay(pdMS_TO_TICKS( 600 ));
  }
}

void task2_events() {
  while(1) {
      uint32_t my_random = esp_random()%6;
    /* alege aleator ce biti sunt setati */
    xEventGroupSetBits(grup, my_random);
    /* asteapta o perioada de timp aleatoare intre 100 si 1500 msec */
    vTaskDelay(pdMS_TO_TICKS( 600 ));
  }
}