#include <stdio.h>
#include "driver/gpio.h"
#include "rtos_tasks.h"



// variable


// functions
void setup_tasks()
{
    grup = xEventGroupCreate();
    xSemaphore = xSemaphoreCreateMutex();
    //xTaskCreate(task1, "t1", 2048, 0, 1, &task1_handle);
    //xTaskCreate(task2, "t2", 2048, 0, 1, &task2_handle);
    //xTaskCreate(task1_events, "t1_ev", 2048, 0, 1, &task1_ev_handle);
    //xTaskCreate(task2_events, "t2_ev", 2048, 0, 1, &task2_ev_handle);
    xTaskCreate(task1_semaphore, "t1_semaphore", 2048, 0, 1, NULL);
    xTaskCreate(task2_semaphore, "t2_semaphore", 2048, 0, 1, NULL);
}


// main
void app_main()
{
    gpio_pad_select_gpio(19);
    gpio_set_direction(19, GPIO_MODE_OUTPUT);
    gpio_pad_select_gpio(18);
    gpio_set_direction(18, GPIO_MODE_OUTPUT);

    setup_tasks();

    while(1)
    {
        vTaskDelay(pdMS_TO_TICKS( 1000 ));
    }
}