/* WiFi station Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "nvs_flash.h"

#include "lwip/err.h"
#include "lwip/sys.h"

#include "connect_wifi.h"
#include "rtos_tasks.h"

/* The examples use WiFi configuration that you can set via 'make menuconfig'.

   If you'd rather not, just change the below entries to strings with
   the config you want - ie #define EXAMPLE_WIFI_SSID "mywifissid"
*/
#define EXAMPLE_ESP_WIFI_SSID      "lab-iot"
#define EXAMPLE_ESP_WIFI_PASS      "IoT-IoT-IoT"
#define EXAMPLE_ESP_MAXIMUM_RETRY  5

/* FreeRTOS event group to signal when we are connected*/
static EventGroupHandle_t s_wifi_event_group;

/* The event group allows multiple bits for each event, but we only care about one event 
 * - are we connected to the AP with an IP? */
const int WIFI_CONNECTED_BIT = BIT0;

static const char *TAG = "wifi station";

static int s_retry_num = 0;

static esp_err_t event_handler(void *ctx, system_event_t *event)
{
    switch(event->event_id) {
    case SYSTEM_EVENT_STA_START:
        esp_wifi_connect();
        break;
    case SYSTEM_EVENT_STA_GOT_IP:
        ESP_LOGI(TAG, "got ip:%s",
                 ip4addr_ntoa(&event->event_info.got_ip.ip_info.ip));
        s_retry_num = 0;
        xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
        break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
        {
            if (s_retry_num < EXAMPLE_ESP_MAXIMUM_RETRY) {
                esp_wifi_connect();
                xEventGroupClearBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
                s_retry_num++;
                ESP_LOGI(TAG,"retry to connect to the AP");
            }
            ESP_LOGI(TAG,"connect to the AP fail\n");
            break;
        }
    default:
        break;
    }
    return ESP_OK;
}

void wifi_init_sta()
{
    s_wifi_event_group = xEventGroupCreate();

    tcpip_adapter_init();
    ESP_ERROR_CHECK(esp_event_loop_init(event_handler, NULL) );

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));
    wifi_config_t wifi_config = {
        .sta = {
            .ssid = EXAMPLE_ESP_WIFI_SSID,
            .password = EXAMPLE_ESP_WIFI_PASS
        },
    };

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA) );
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config) );
    ESP_ERROR_CHECK(esp_wifi_start() );

    ESP_LOGI(TAG, "wifi_init_sta finished.");
    ESP_LOGI(TAG, "connect to ap SSID:%s password:%s",
             EXAMPLE_ESP_WIFI_SSID, EXAMPLE_ESP_WIFI_PASS);
}

void connect_to_wifi()
{
        //Initialize NVS
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);
    
    ESP_LOGI(TAG, "ESP_WIFI_MODE_STA");
    wifi_init_sta();
}

static int sock;

void create_UDP_comm()
{
    sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
    struct sockaddr_in addr;

    addr.sin_addr.s_addr = htonl(INADDR_ANY); // adresa IP 0.0.0.0
    addr.sin_family = AF_INET;
    addr.sin_port = htons(8888);              // numărul de port 8888
    int err = bind(sock, (struct sockaddr *)&addr, sizeof(addr));    
}

int send_tx_udp(char tx_buffer[128], char ip_addr[])
{
    // to send
    struct sockaddr_in destAddr;  // ... inițializată similar cu structura de mai sus
    inet_aton(ip_addr, &destAddr.sin_addr);
    destAddr.sin_family = AF_INET;
    destAddr.sin_port = htons(8888);              // numărul de port 8888

    int err = sendto(sock, tx_buffer, sizeof(tx_buffer) + 1, 0, (struct sockaddr *)&destAddr, sizeof(destAddr));

    ESP_LOGI(TAG, "Packet %s sent to %s\n", tx_buffer, ip_addr);

    return err;
}

int receive_rx_udp(char rx_buffer[128])
{
    struct sockaddr_in sourceAddr; 
    socklen_t socklen = sizeof(sourceAddr);
    int len = recvfrom(sock, rx_buffer, sizeof(rx_buffer) - 1, 0, (struct sockaddr *)&sourceAddr, &socklen);
    ESP_LOGI(TAG, "Packet %s received from %d\n", rx_buffer, sourceAddr.sin_addr.s_addr);
    return len;
}



// functions
void setup_tasks()
{
    xTaskCreate(task1, "t1", 2048, 0, 1, &task1_handle);
    xTaskCreate(task2, "t2", 2048, 0, 1, &task2_handle);
}




void app_main()
{
    connect_to_wifi();
    create_UDP_comm();
    setup_tasks();
    while(1)
    {
        vTaskDelay(2000 / portTICK_PERIOD_MS);
    }

}