
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "freertos/semphr.h"



// variables
static TaskHandle_t task1_handle = NULL;
static TaskHandle_t task2_handle = NULL;


// functions
void task1();
void task2();
