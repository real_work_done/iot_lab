#include "rtos_tasks.h"

void task1() {
  uint32_t ret;
  while(1) {
    vTaskDelay(20000 / portTICK_PERIOD_MS);
  }
}

void task2() {
  uint32_t val=0;
  while(1) {
    vTaskDelay(2000 / portTICK_PERIOD_MS);
  }
}
