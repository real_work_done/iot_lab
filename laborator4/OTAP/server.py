import io
from flask import Flask, send_file

app = Flask(__name__)
@app.route('/version')
def get_version():
    return '3'

@app.route('/firmware.bin')
def firm():
    # TODO: Modificati calea catre binar, in functie de platforma aleasa la crearea proiectului
    with open("./.pio/build/esp32doit-devkit-v1/firmware.bin", 'rb') as bites:
        return send_file(
                     io.BytesIO(bites.read()), as_attachment=True, attachment_filename="firmware.bin"
               )

if __name__ == '__main__':
    app.run(host='0.0.0.0')