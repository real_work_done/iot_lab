#include <esp_wifi.h>
#include <esp_event_loop.h>
#include <esp_log.h>
#include <esp_system.h>
#include <nvs_flash.h>
#include <sys/param.h>
#include <string.h>

#include <esp_http_server.h>

#include "esp_err.h"
#include "esp_log.h"
#include "esp_spiffs.h"

#define EXAMPLE_WIFI_SSID "test__S_1003"
#define EXAMPLE_WIFI_PASS "12345678"

static const char *TAG="APP";
char resp_str[1024] = "", opt[64];

// read from nvm
char passw[32];
char id_log[64];
//
int flag = 0;
int flag_2 = 0;
//

void write_credentials();

static char* getAuthModeName(wifi_auth_mode_t auth_mode) {
	
	char *names[] = {"OPEN", "WEP", "WPA PSK", "WPA2 PSK", "WPA WPA2 PSK", "MAX"};
	return names[auth_mode];
}

void wifi_scan(wifi_ap_record_t *ap_records, uint16_t *ap_num)
{
    ESP_LOGI(TAG, "scanning...");

    wifi_scan_config_t scan_config = {
	    .ssid = 0,
	    .bssid = 0,
	    .channel = 0,
        .show_hidden = false
    };
    ESP_ERROR_CHECK(esp_wifi_scan_start(&scan_config, true));
    *ap_num=20;
    ESP_ERROR_CHECK(esp_wifi_scan_get_ap_records(ap_num, ap_records));
    printf("Found %d access points:\n", *ap_num);
    for(int i = 0; i < *ap_num; i++)
	    printf("%32s | %7d | %4d | %12s\n", 
        (char *)ap_records[i].ssid, ap_records[i].primary, 
        ap_records[i].rssi, getAuthModeName(ap_records[i].authmode));
}

esp_err_t root_get_handler(httpd_req_t *req)
{
    const char* header = 
    "<html>\
    <body>\
    <form action=\"/\" target=\"_blank\" method=\"post\">\
    <label for=\"fname\">Networks found:</label>\
    <br>\
    <select name=\"ssid\">";
  
    const char* footer = "</select>\
    <br>\
    <label for=\"ipass\">Security key:</label><br>\
    <input type=\"password\" name=\"ipass\"><br>\
    <input type=\"submit\" value=\"Submit\">\
    </form>\
    </body>\
    </html>";

    const char* opt_field = "<option value=\"%s\">%s</option>";

    ESP_LOGI(TAG, "GET /");

    wifi_ap_record_t ap_records[20];
    uint16_t ap_num = 20;
    wifi_scan(ap_records, &ap_num);

    strcat(resp_str,header);
    for(int i = 0; i < ap_num; i++)
    {
        sprintf(opt, opt_field, ap_records[i].ssid, ap_records[i].ssid);
        strcat(resp_str, opt);
    }
    strcat(resp_str, footer);

    httpd_resp_send(req, resp_str, strlen(resp_str));
    return ESP_OK; 
}

esp_err_t root_post_handler(httpd_req_t *req)
{
    char *buf;
    int buf_len = req->content_len;
    int ret;

    // to be saved
    char ssid[32];
    char ipass[32];
    //

    const char* resp = "<html>\
    <body>\
    Ok\
    </body>\
    </html>";

    buf = malloc(buf_len);
    ret = httpd_req_recv(req, buf, buf_len);
    if (ret > 0)
    {
        ESP_LOGI(TAG, "=========== RECEIVED DATA ==========");
        ESP_LOGI(TAG, "%.*s", ret, buf);
        ESP_LOGI(TAG, "====================================");

        if (httpd_query_key_value(buf, "ssid", ssid, sizeof(ssid)) == ESP_OK) {
            ESP_LOGI(TAG, "Found URL query parameter => ssid=%s", ssid);
        }
        if (httpd_query_key_value(buf, "ipass", ipass, sizeof(ipass)) == ESP_OK) {
            ESP_LOGI(TAG, "Found URL query parameter => ipass=%s", ipass);
        }

        httpd_resp_send(req, resp, strlen(resp));
    }

    memcpy(id_log, ssid, sizeof(ssid));
    memcpy(passw, ipass, sizeof(ipass));
    write_credentials();
    flag_2=1;
    return ESP_OK; 
}

httpd_uri_t root_put = {
    .uri       = "/",
    .method    = HTTP_POST,
    .handler   = root_post_handler,
    .user_ctx  = NULL
};

httpd_uri_t root_get = {
    .uri       = "/",
    .method    = HTTP_GET,
    .handler   = root_get_handler,
    .user_ctx  = NULL
};

httpd_handle_t start_webserver(void)
{
    httpd_handle_t server = NULL;
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();

    // Start the httpd server
    ESP_LOGI(TAG, "Starting server on port: '%d'", config.server_port);
    if (httpd_start(&server, &config) == ESP_OK) {
        // Set URI handlers
        ESP_LOGI(TAG, "Registering URI handlers");
        httpd_register_uri_handler(server, &root_put);
        httpd_register_uri_handler(server, &root_get);
        return server;
    }

    ESP_LOGI(TAG, "Error starting server!");
    return NULL;
}

void stop_webserver(httpd_handle_t server)
{
    // Stop the httpd server
    httpd_stop(server);
}

static esp_err_t event_handler(void *ctx, system_event_t *event)
{
    httpd_handle_t *server = (httpd_handle_t *) ctx;

    switch(event->event_id) {
    case SYSTEM_EVENT_STA_START:
        ESP_LOGI(TAG, "SYSTEM_EVENT_STA_START");
        ESP_ERROR_CHECK(esp_wifi_connect());
        break;
    case SYSTEM_EVENT_STA_GOT_IP:
        ESP_LOGI(TAG, "SYSTEM_EVENT_STA_GOT_IP");
        ESP_LOGI(TAG, "Got IP: '%s'",
                ip4addr_ntoa(&event->event_info.got_ip.ip_info.ip));

        
        break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
        ESP_LOGI(TAG, "SYSTEM_EVENT_STA_DISCONNECTED");
        ESP_ERROR_CHECK(esp_wifi_connect());

        // todo reset ecu ? 
        break;
    /////////////////////////
    case SYSTEM_EVENT_AP_STOP:
        ESP_LOGI(TAG, "SYSTEM  AP STA STOP");
    
        /* Stop the web server */
        if (*server) {
            stop_webserver(*server);
            *server = NULL;
        }
        break;
    case SYSTEM_EVENT_AP_START:
            ESP_LOGI(TAG, "SYSTEM  AP STA START");
        
                break;
        case SYSTEM_EVENT_AP_STACONNECTED:
            ESP_LOGI(TAG, "SYSTEM EVENT AP STA CONNECTED");
                /* If there are no credentials in NvM */
            /* Start the web server */
            if (*server == NULL) {
                *server = start_webserver();
            }
        
                break;
        case  SYSTEM_EVENT_AP_STADISCONNECTED:
            ESP_LOGI(TAG, "SYSTEM EVENT AP STA DISCONNECTED");
                 break;
    ////////////////////////
    default:
        break;
    }
    return ESP_OK;
}

static void initialise_wifi(void *arg)
{
    tcpip_adapter_init();
    ESP_ERROR_CHECK(esp_event_loop_init(event_handler, arg));
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));
    ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));

    // if we don't have values already
    wifi_config_t wifi_config;
    if(flag == 0)
    {
        sprintf((char*)id_log, "%s", EXAMPLE_WIFI_SSID);
        sprintf((char*)passw, "%s", EXAMPLE_WIFI_PASS);

        
        memcpy(wifi_config.ap.ssid, (uint8_t*)id_log, sizeof(id_log));
        memcpy(wifi_config.ap.password, (uint8_t*)passw, sizeof(passw));
        wifi_config.ap.max_connection = 4;
        wifi_config.ap.authmode = WIFI_AUTH_WPA_WPA2_PSK;
        wifi_config.ap.ssid_len = strlen(id_log);
        wifi_config.ap.channel = 7;
    }
    else{

        memcpy(wifi_config.sta.ssid, (uint8_t*)id_log, sizeof(id_log));
        memcpy(wifi_config.sta.password, (uint8_t*)passw, sizeof(passw));
    }
    

    ESP_LOGI(TAG, "Setting WiFi configuration SSID %s...", wifi_config.sta.ssid);

    if (flag == 1){
        ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
        ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config));
    }
    else{
        ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_AP));
        ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_AP, &wifi_config));
    }
    ESP_ERROR_CHECK(esp_wifi_start());
}

void write_credentials()
{
    // Use POSIX and C standard library functions to work with files.
    // First create a file.

    ESP_LOGI(TAG, "Opening file");
    FILE* f = fopen("/spiffs/logging_credentials.txt", "w");
    if (f == NULL) {
        ESP_LOGE(TAG, "Failed to open file for writing");
        return;
    }

    ESP_LOGI(TAG, "Saving data to NvM! \r\n");
    fprintf(f, "%s\n%s", passw, id_log);
    fclose(f);
}

void setup_spiffs()
{
    ESP_LOGI(TAG, "Initializing SPIFFS");
    esp_vfs_spiffs_conf_t conf = {
      .base_path = "/spiffs",
      .partition_label = NULL,
      .max_files = 5,
      .format_if_mount_failed = true
    };

    // Use settings defined above to initialize and mount SPIFFS filesystem.
    // Note: esp_vfs_spiffs_register is an all-in-one convenience function.
    esp_err_t ret = esp_vfs_spiffs_register(&conf);

    if (ret != ESP_OK) {
        if (ret == ESP_FAIL) {
            ESP_LOGE(TAG, "Failed to mount or format filesystem");
        } else if (ret == ESP_ERR_NOT_FOUND) {
            ESP_LOGE(TAG, "Failed to find SPIFFS partition");
        } else {
            ESP_LOGE(TAG, "Failed to initialize SPIFFS (%s)", esp_err_to_name(ret));
        }
        return;
    }

       // Check if destination file exists before renaming

    struct stat st;
    if (stat("/spiffs/logging_credentials.txt", &st) == 0) {
        // The file exists
        // Open renamed file for reading

        ESP_LOGI(TAG, "File exists! Reading file");
        FILE* f = fopen("/spiffs/logging_credentials.txt", "r");
        if (f == NULL) {
            ESP_LOGE(TAG, "Failed to open file for reading");
            return;
        }

        fgets(id_log, sizeof(id_log), f);
        fgets(passw, sizeof(passw), f);
        flag = 1;
        flag_2 = 2;
        fclose(f);
        ESP_LOGI(TAG, "Found %s && %s \r\n", id_log, passw);
        unlink("/spiffs/logging_credentials.txt");

    }
    else{
        flag = 0;
    }
}

void app_main()
{
    static httpd_handle_t server = NULL;
    ESP_ERROR_CHECK(nvs_flash_init());

    setup_spiffs();

    initialise_wifi(&server);

    while(flag_2 == 0)
    {
        vTaskDelay(1 * 1000 / portTICK_RATE_MS);
    }
    if(flag_2 == 1)
    {
        esp_restart();  
    }

    ESP_LOGI(TAG, "No need to soft reset ! \r\n");   
    while(1)
    {
        vTaskDelay(1 * 1000 / portTICK_RATE_MS);
        if(flag_2 == 1)
        {
            esp_restart();
        }
    }   
}