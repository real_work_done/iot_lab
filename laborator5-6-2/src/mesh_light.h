#ifndef __MESH_LIGHT_H__
#define __MESH_LIGHT_H__

#include "esp_err.h"

/*******************************************************
 *                Constants
 *******************************************************/
#define MESH_LIGHT_RED       (0xff)
#define MESH_LIGHT_GREEN     (0xfe)
#define MESH_LIGHT_BLUE      (0xfd)
#define MESH_LIGHT_YELLOW    (0xfc)
#define MESH_LIGHT_PINK      (0xfb)
#define MESH_LIGHT_INIT      (0xfa)
#define MESH_LIGHT_WARNING   (0xf9)

#define  MESH_TOKEN_ID       (0x0)
#define  MESH_TOKEN_VALUE    (0xbeef)
#define  MESH_CONTROL_CMD    (0x2)

#define CONFIG_MESH_ROUTE_TABLE_SIZE 20 // dimenisunea maxima a tabelei de rutare
#define CONFIG_MESH_MAX_LAYER 3 // numarul maxim de niveluri ale arborelui
#define CONFIG_MESH_CHANNEL 1 // canalul WiFi pe care se creaza reteaua mesh (trebuie sa coincida cu canalul AP-ului !)
#define CONFIG_MESH_ROUTER_SSID "lab-iot" // SSID-ul AP-ului la care se va conecta nodul root
#define CONFIG_MESH_ROUTER_PASSWD "IoT-IoT-IoT" // parola
#define CONFIG_MESH_AP_AUTHMODE WIFI_AUTH_WPA_WPA2_PSK // metoda de securitate folosita pentru interfetele softAP
#define CONFIG_MESH_AP_CONNECTIONS 3 // numarul maxim de noduri fiu
#define CONFIG_MESH_AP_PASSWD "12345678" // parola pentru conectarea la interfata softAP (ptr WPA2 min 8 caractere)


/*******************************************************
 *                Type Definitions
 *******************************************************/

/*******************************************************
 *                Structures
 *******************************************************/
typedef struct {
    uint8_t cmd;
    bool on;
    uint8_t token_id;
    uint16_t token_value;
} mesh_light_ctl_t;

/*******************************************************
 *                Variables Declarations
 *******************************************************/

/*******************************************************
 *                Function Definitions
 *******************************************************/
esp_err_t mesh_light_init(void);
esp_err_t mesh_light_set(int color);
esp_err_t mesh_light_process(mesh_addr_t *from, uint8_t *buf, uint16_t len);
void mesh_connected_indicator(int layer);
void mesh_disconnected_indicator(void);

#endif /* __MESH_LIGHT_H__ */