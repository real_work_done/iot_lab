import socket

def send_packet(secret):
    try:
        broadcast_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        #broadcast_socket.bind(('', 0))
        broadcast_socket.setsockopt(socket.SOL_SOCKET, IN.SO_BINDTODEVICE, "eth0")
        broadcast_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    except Exception as err:
        print ("[!] Error creating broadcast socket: %s" % err)
    
    data = "nukemyluks_" + secret
    try:
        broadcast_socket.sendto(data.encode(), ('255.255.255.255', 6767))
        
    except Exception as err:
        print ("[!] Error sending packet: %s" % err)
'''
try:
    cs = socket(AF_INET, SOCK_DGRAM)
    cs.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
    cs.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)
except Exception as err:
    print("ERROR!")
   

cs.sendto('This is a test'.encode(), ('255.255.255.255', 6767))
'''

send_packet("HELLO!")