#ifndef WIFI_CONNECTED_HEADER
#define WIFI_CONNECTED_HEADER


#include "project_defines.h"
#include "esp_mesh.h"

void connect_to_wifi();
void create_UDP_comm();
int send_tx_udp(char tx_buffer[]);
int receive_rx_udp(char rx_buffer[]);
int send_tx_udp_to_ip(uint8_t tx_buffer[], mesh_addr_t ip_address_des);
#ifdef MESH_ID_1
    #define BROADCAST_PORT  6767
    #define RECEIVE_PORT    6768
#else
    #define BROADCAST_PORT  6768
    #define RECEIVE_PORT    6767
#endif

#define TEST_P2P_PORT        6625


#endif